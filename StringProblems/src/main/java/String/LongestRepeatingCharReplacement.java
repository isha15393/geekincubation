package String;

public class LongestRepeatingCharReplacement {

    public static int findLongestRepeatingCharReplacement(String s, int k) {
        int i=1;
        int max = -1, len = 1, initK = k, repIndx = -1;
        char[] arr = s.toCharArray();
        char c = arr[0];
        while(i<arr.length) {
            if(arr[i] != c) {
                if(k>0) {
                    len++;k--;
                    if(repIndx == -1) {
                        repIndx = i;
                    }
                    i++;
                } else {
                    if(len>max) {
                        max = len;
                    }
                    len = 1;
                    if(repIndx == -1) { repIndx = i;}
                    c = arr[repIndx];
                    k = initK;
                    i = repIndx +1;
                    repIndx = -1;
                }
            } else if (arr[i] == c) {
                len++;
                i++;
            }
        }
        if(len>max) {
            max = len;
        }
        return max;
    }
    public static void main(String[] args) {
        System.out.println("Longest substring length after replacement in word 'AABABBA' is "+findLongestRepeatingCharReplacement("AABABBA",1));
        System.out.println("Longest substring length after replacement in word 'ABAB' is "+findLongestRepeatingCharReplacement("ABAB",2));
        System.out.println("Longest substring length after replacement in word 'AAAB' is "+findLongestRepeatingCharReplacement("AAAB",0));
        //System.out.println("Longest substring length after replacement in word 'ABBB' is "+findLongestRepeatingCharReplacement("ABBB",2)); --- failing testCases

    }
}
