package String;

public class LongestUniformSubString {

    public static int[] longestUniformSubstring(String input) {
        char[] arr = input.toCharArray();
        int maxLength = Integer.MIN_VALUE;
        int startIndex = -1, endIndex = -1;
        int tempStart = 0;
        int tempEnd = -1;
        int length = 1;
        for(int i=0;i<arr.length-1;i++) {
            if(arr[i] == arr[i+1]) {
                length++;
            } else {
                tempEnd = i;
                if(length>maxLength) {
                    maxLength = length;
                    startIndex = tempStart;
                    endIndex = tempEnd;
                }
                tempStart = i+1;
                length = 1;
            }
        }
        return new int[]{startIndex,endIndex};
    }

    public static void main(String[] args) {
        int[] arr = longestUniformSubstring("aabbbbcdd");
        System.out.println("Index of Longest Uniform substring is "+arr[0]+ " , "+arr[1]);
        arr = longestUniformSubstring("abbbccda");
        System.out.println("Index of Longest Uniform substring is "+arr[0]+ " , "+arr[1]);
    }
}
