package String;

public class ReverseString {

    public static String reverseString(String input) {
        char[] charArr = input.toCharArray();
        StringBuffer buffer = new StringBuffer();
        for (int i = charArr.length-1;i>=0;i--) {
            buffer.append(charArr[i]);
        }
        return buffer.toString();
    }

    public static void main(String[] args) {
        System.out.println("Reverse String of aabccd is "+reverseString("aabccd"));
    }
}
