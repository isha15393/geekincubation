package String;

import java.util.LinkedHashMap;
import java.util.Map;

public class FirstNonRepeatingCharacter {

    public static Character firstNonRepeatingChar(String input) {
        Map<Character,Integer> freqMap = new LinkedHashMap<>();
        char[] word = input.toCharArray();
        for(Character c : word) {
            freqMap.put(c,freqMap.getOrDefault(c,0)+1);
        }
        for(Character c : freqMap.keySet()) {
            if(freqMap.get(c) == 1) {
                return c;
            }
        }
        return null;
    }
    public static void main(String[] args) {
        System.out.println("First non-repeating char in string 'Toodles' is "+firstNonRepeatingChar("Toodles"));
        System.out.println("First non-repeating char in string 'ookkbbyyee' is "+firstNonRepeatingChar("ookkbbyyee"));
        System.out.println("First non-repeating char in string 'ookkbbyee' is "+firstNonRepeatingChar("ookkbbyee"));

    }

}
