package String;

public class LongestIncreasingSubsequence {

    public static int longestIncreasingSubSeqLength(int[] arr) {
        int[] freqArr = new int[arr.length];
        int lis = Integer.MIN_VALUE;
        for(int i=0;i<freqArr.length;i++) {
            freqArr[i] = 1;
        }
        int j=0;
        while(j<=arr.length) {
            int i = j+1;
            while(i<arr.length) {
                if ((arr[i] > arr[j]) && (freqArr[i] <= freqArr[j])) {
                    freqArr[i] = freqArr[j] + 1;
                } else {
                    i++;
                }
            }
            j++;
        }

        for(int k=0;k<freqArr.length;k++) {
            if(freqArr[k] > lis) {
                lis = freqArr[k];
            }
        }
        return lis;
    }

    public static int longestIncreasingSubSeqLengthModified(int[] arr) {
        int[] tempArr = new int[arr.length];
        for(int i=0;i<tempArr.length;i++) {
            tempArr[i] = 1;
        }
        int lis = Integer.MIN_VALUE;
        int i = 0;
        int j = i+1;
        while( j <= arr.length) {

        }
        return lis;
    }

    public static void main(String[] args) {
        int[] input = new int[]{50,3,10,7,40,80};
        System.out.println("LIS value for the provided Arr is "+longestIncreasingSubSeqLength(input));
        input = new int[]{3,10,2,1,20};
        System.out.println("LIS value for the provided Arr is "+longestIncreasingSubSeqLength(input));
    }
}
