package String;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ShortestDistanceBetweenCharacters {

    public static int[] shortestDistanceArray(String word, Character x) {

        char[] arr = word.toCharArray();
        List<Integer> indexList = new ArrayList<>();
        for(int i=0;i<arr.length;i++) {
            if(arr[i] == x ) {
                indexList.add(i);
            }
        }

        int[] output = new int[word.length()];
        output[0] = indexList.get(0);

        for(int j=1;j<arr.length;j++) {
            int tempDis = Integer.MAX_VALUE;
            for(int k=0;k<indexList.size();k++) {
                if(indexList.get(k)-j == 0) {
                    tempDis = 0;
                    break;
                } else if(Math.abs(indexList.get(k)-j)<tempDis) {
                    tempDis = Math.abs(indexList.get(k)-j);
                }
            }
            output[j] = tempDis;
        }
        return output;
    }

    public static void main(String[] args) {
        System.out.println("Shortest Distance Array 'geeksforgeeks' :");
        int[] arr = shortestDistanceArray("geeksforgeeks",'e');
        for(int i = 0;i<arr.length;i++) {
            System.out.print(" " + arr[i]);
        }
        System.out.println("");
        System.out.println("Shortest Distance Array 'helloworld' :");
        arr = shortestDistanceArray("helloworld",'o');
        for(int i = 0;i<arr.length;i++) {
            System.out.print(" " + arr[i]);
        }
    }

}
