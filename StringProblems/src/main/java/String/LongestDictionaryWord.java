package String;

import java.util.HashSet;
import java.util.Set;

public class LongestDictionaryWord {

    /* Assuming dictionary to be in sorted order as present in the dictionary.
    * */
    public static Set<String> longestWord(String letters, String[] dictionary) {
        char[] charArr = letters.toCharArray();
        Set<Character> ch = new HashSet<>();
        for(int i=0;i<charArr.length;i++) {
            ch.add(charArr[i]);
        }
        Set<String> output = new HashSet<>();
        int longestLength = Integer.MIN_VALUE;
        for(int i=dictionary.length-1;i>0;i--) {
            char[] words = dictionary[i].toCharArray();
            for(int j=0;j<words.length;j++) {
                if(!ch.contains(words[j])){
                    break;
                }
                if(dictionary[i].length()>=longestLength) {
                    longestLength = dictionary[i].length();
                    output.add(dictionary[i]);
                }
            }
        }
        return output;
    }


    public static void main(String[] args) {
        String[] dictionary = {"to","toe","tseo","toes"};
        Set<String> longestWords = longestWord("oet",dictionary);
        System.out.println("Longest Words in the provided dictionary are ");
        longestWords.stream().forEach(st->System.out.println(st));
    }
}
