package String;

import java.util.LinkedList;
import java.util.List;

public class ShortestConsequetiveSubstring {

    public static String shortestString(String input) {
        List<String> output = new LinkedList<>();
        char[] arr = input.toCharArray();
        char temp = arr[0];
        StringBuffer tempBuf = new StringBuffer();
        tempBuf.append(String.valueOf(temp));
        int l = 1;
        int min= Integer.MAX_VALUE;
        for(int i=1;i<arr.length;i++){
            if(arr[i] == temp) {
                l++;
                tempBuf.append(String.valueOf(temp));
            }else {
                if(l<min) {
                    min = l;
                    output.clear();
                    output.add(String.valueOf(tempBuf));
                }else if(l == min) {
                    output.add(String.valueOf(tempBuf));
                }
                l=1;
                temp = arr[i];
                tempBuf = new StringBuffer();
                tempBuf.append(temp);
            }
        }

        if(l<min) {
            output.clear();
            output.add(String.valueOf(tempBuf));
        } else if (l == min) {
            output.add(String.valueOf(tempBuf));
        }
        StringBuffer result = new StringBuffer();
        output.stream().forEach(str->result.append(str));
        return result.toString();
    }
    public static void main(String[] args) {
        System.out.println("The Shortest Substring present in aaaabbbcc is "+shortestString("aaaabbbcc"));
        System.out.println("The Shortest Substring present in aabbcc is "+shortestString("aabbcc"));
        System.out.println("The Shortest Substring present in abc is "+shortestString("abc"));
        System.out.println("The Shortest Substring present in abbccc is "+shortestString("abbccc"));
        System.out.println("The Shortest Substring present in abbc is "+shortestString("abbc"));


    }
}
