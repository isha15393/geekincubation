package String;

import java.util.HashMap;
import java.util.Map;

public class ApacheLogPattern {

    public static String findTopIpaddress(String[] lines)
    {
        Map<String, Integer> ipFreqMap = new HashMap<>();
        for(int i=0;i<lines.length;i++) {
            String ip = lines[i].split(" - ")[0];
            if(ipFreqMap.get(ip) == null) {
                ipFreqMap.put(ip,1);
            } else {
                ipFreqMap.put(ip,ipFreqMap.get(ip)+1);
            }
        }
        int max = Integer.MIN_VALUE;
        String maxLocatedIP = "";
        for(String ips : ipFreqMap.keySet()) {
            int val = ipFreqMap.get(ips);
            if(val>max) {
                max = val;
                maxLocatedIP = ips;
            }
        }
        return maxLocatedIP;
    }

    public static void main(String[] args) {
        String lines[] = new String[]{
                "10.0.0.1 - frank [10/Dec/2000:12:34:56 -0500] \"GET /a.gif HTTP/1.0\" 200 234",
                "10.0.0.1 - frank [10/Dec/2000:12:34:57 -0500] \"GET /b.gif HTTP/1.0\" 200 234",
                "10.0.0.2 - nancy [10/Dec/2000:12:34:58 -0500] \"GET /c.gif HTTP/1.0\" 200 234"};
        System.out.println("Most Frequent IP Address is "+findTopIpaddress(lines));

        String  lines2[] = new String[]{
                "10.0.0.1 - frank [10/Dec/2000:12:34:56 -0500] \"GET /a.gif HTTP/1.0\" 200 234",
                "10.0.0.2 - frank [10/Dec/2000:12:34:57 -0500] \"GET /b.gif HTTP/1.0\" 200 234",
                "10.0.0.2 - nancy [10/Dec/2000:12:34:58 -0500] \"GET /c.gif HTTP/1.0\" 200 234",
                "10.0.0.2 - nancy [10/Dec/2000:12:34:59 -0500] \"GET /c.gif HTTP/1.0\" 200 234",
                "10.0.0.3 - logan [10/Dec/2000:12:34:59 -0500] \"GET /d.gif HTTP/1.0\" 200 234",};
        System.out.println("Most Frequent IP Address is "+findTopIpaddress(lines2));

    }
}
