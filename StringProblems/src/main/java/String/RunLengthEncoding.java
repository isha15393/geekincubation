package String;

public class RunLengthEncoding {

    public static String encodeTheString(String input) {
        char[] inpAr = input.toCharArray();
        StringBuffer buffer = new StringBuffer();
        char c = inpAr[0];
        int count = 1;
        for(int i=1;i<inpAr.length;i++) {
            if(c == inpAr[i]) {
                count++;
            }else{
                buffer.append(Character.toString(c)+count);
                c = inpAr[i];
                count = 1;
            }
        }
        buffer.append(Character.toString(c)+count);
        return buffer.toString();
    }

    public static void main(String[] args) {
        System.out.println("Encoded string is "+encodeTheString("aaabbcdddd"));
        System.out.println("Encoded string is "+encodeTheString("aaabbbaad"));

    }
}
