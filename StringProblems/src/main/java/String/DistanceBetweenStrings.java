package String;

public class DistanceBetweenStrings {

    public static double shortestDistance(String document, String word1, String word2) {
        double wordlength1 = word1.length()/2.0;
        double wordLength2 = word2.length()/2.0;
        String[] arr = document.split(" ");
        int charLength = 0;
        int wordsBtw = 0;
        for(int i=0;i<arr.length;i++) {
            if(arr[i].equals(word1)) {
               for(int j=i+1;j<arr.length;j++){
                   if(arr[j].equals(word2)){
                       break;
                   }else{
                       wordsBtw++;
                       charLength = charLength + arr[j].length();
                   }
               }
               return wordlength1+wordLength2+charLength+(wordsBtw+1);
            }
        }
        return 0;
    }


    public static void main(String[] args) {

        StringBuffer sb = new StringBuffer();
        sb.append("In publishing and graphic design ,lorem ipsum is a filler text commonly used to demonstrate the graphic elements");
        sb.append(" lorem ipsum text has been used in typesetting since the 1960s or earlier, when it was popularized by advertisements");
        sb.append(" for Letraset transfer sheets. It was introduced to the Information Age in the mid-1980s by Aldus Corporation, which");

        String document = sb.toString();
        System.out.println("Shortest Distance btw 'is' and 'a'= "+shortestDistance(document,"is","a"));
        System.out.println("Shortest Distance btw 'and' and 'graphic'= "+shortestDistance(document,"and","graphic"));
        System.out.println("Shortest Distance btw 'transfer' and 'It'= "+shortestDistance(document,"transfer","It"));
        System.out.println("Shortest Distance btw 'design' and 'filler'= "+shortestDistance(document,"design","filler"));

    }
}
