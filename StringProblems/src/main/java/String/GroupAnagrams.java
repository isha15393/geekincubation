package String;

import java.util.*;

public class GroupAnagrams {

    public static Collection<Set<String>> groups(List<String> words) {
        Set<Set<String>> output = new HashSet<>();
        Map<Integer,Set<String>> map = new HashMap<>();
        for(String word: words) {
            char[] arr = word.toCharArray();
            int sum = 0;
            for(int i=0;i<arr.length;i++){
                sum = sum + arr[i];
            }
            Set<String> val;
            if(map.get(sum) == null) {
                 val = new HashSet<>();

            }else{
                 val = map.get(sum);
            }
            val.add(word);
            map.put(sum,val);
        }
        return  map.values();
    }
    public static void main(String[] args) {
        Collection<Set<String>> anagrams = groups(Arrays.asList("dog","god","cat","pop","opp"));
        anagrams.forEach(s->{
            System.out.print("{");
            StringJoiner str = new StringJoiner(",");
            s.forEach(x->str.add(x));
            System.out.print(str.toString());
            System.out.println("}");
            });
    }
}
