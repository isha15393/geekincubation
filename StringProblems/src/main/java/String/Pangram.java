package String;

import java.util.Locale;

public class Pangram {

    public static String findMissingAlphabets(String input) {
       String stringWithoutBlank = input.replaceAll(" ","");
       char[] inputArr = stringWithoutBlank.toLowerCase(Locale.ROOT).toCharArray();
       int[] countArr = new int[26];
       // lowercase : 97 to 122, space = 32, uppercase: 65 to 90, digits : 48 to 57
       for(int i=0;i<inputArr.length;i++) {
               countArr[inputArr[i] - 97] = -1;
       }

       StringBuffer buffer = new StringBuffer();
       for(int i=0;i<countArr.length;i++) {
           if(countArr[i] != -1) {
               buffer.append((char) (i + 97));
           }
       }
       return buffer.toString();
    }

    public static void main(String[] args) {
        System.out.println("Missing alphabets in string to make it a panagram is "
                +findMissingAlphabets("The slow purple oryx meanders past the quiescent canine"));
    }
}
