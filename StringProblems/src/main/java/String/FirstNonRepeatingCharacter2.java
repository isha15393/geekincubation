package String;

public class FirstNonRepeatingCharacter2 {

    public static int findIndexOfFirstNonRepeatingCharacter(String input) {
        char[] arr = input.toCharArray();
        int[] countArr = new int[256];
        for(int i=0;i<countArr.length;i++) {
            countArr[i] = -1;
        }
        for(int j=0;j<arr.length;j++) {
            if(countArr[arr[j]] == -1) {
                countArr[arr[j]] = j;
            } else if (countArr[arr[j]] >= 0) {
                countArr[arr[j]] = -2;
            }
        }

        int firstIndex = Integer.MAX_VALUE;
        for(int k=0;k<countArr.length;k++) {
            if(countArr[k]>=0) {
                firstIndex = Math.min(firstIndex,countArr[k]);
            }
        }
        if(firstIndex == Integer.MAX_VALUE)
            return -1;

        return firstIndex;
    }


    public static void main(String[] args) {
        System.out.println("Index of First nonrepeating char in word 'Toodles' is "+findIndexOfFirstNonRepeatingCharacter("Toodles"));
        System.out.println("Index of First nonrepeating char in word 'Cry' is "+findIndexOfFirstNonRepeatingCharacter("Cry"));
        System.out.println("Index of First nonrepeating char in word 'wwiinnd' is "+findIndexOfFirstNonRepeatingCharacter("wwiinnd"));

    }
}
