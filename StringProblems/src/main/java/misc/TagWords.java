package misc;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class TagWords {

    public static String encodeWithTags(String input,Map<String,String> tag) {
        for (String word:tag.keySet()) {
            String lowerInput = input.toLowerCase(Locale.ROOT);
            int index = lowerInput.indexOf(word.toLowerCase(Locale.ROOT));
            if(index != -1) {
                String part1 = input.substring(0,index);
                String part2 = input.substring(index+word.length(),input.length());
                input = part1 + " [" + tag.get(word) + "] "+"{" +word +"} "+ part2;
            }
        }
        return input;
    }
    public static void main(String[] args) {
        String input = "I visited San Francisco for work and stayed at Airbnb." +
                "I really loved the city and the home where I stayed.";
        Map<String,String> codedWords = new HashMap<>();
        codedWords.put("airbnb","business");
        codedWords.put("san francisco","city");
        System.out.println("Encoded String is - "+encodeWithTags(input,codedWords));

        input = "I travelled to San Francisco for work and stayed at Airbnb." +
                "I really loved the city and the home where I stayed." +
                "I stayed with San and Francisco." +
                "They both were really good and san's hospitality was outstanding.";
        codedWords = new HashMap<>();
        codedWords.put("san","person");
        codedWords.put("francisco","person");
        codedWords.put("san francisco","city");
        codedWords.put("Airbnb","business");
        codedWords.put("city","location");

        System.out.println("Encoded String is - "+encodeWithTags(input,codedWords));
    }
}
