package misc;

import java.util.*;

public class TaggingService {

    static class Tags{

        Map<String,List<String>> productTagMap;
        Map<String,List<String>> tagProductMap;
        public Tags() {
            productTagMap = new Hashtable<>();
            tagProductMap = new Hashtable<>();
        }

        public void attachTagToProduct(String tag, String product){
                if(productTagMap.containsKey(product)){
                    productTagMap.get(product).add(tag);
                }else {
                    productTagMap.put(product, new ArrayList<>(Arrays.asList(tag)));
                }

                if(tagProductMap.containsKey(tag)){
                    tagProductMap.get(tag).add(product);
                }else {
                    tagProductMap.put(tag,new ArrayList<>(Arrays.asList(product)));
                }
        }

        public List<String> getProductsByTag(String tag){
            return tagProductMap.get(tag);
        }

        public List<String> getTagsAssociatedToProduct(String product) {
            return productTagMap.get(product);
        }
    }
    public static void main(String[] args){
        String[] products = new String[]{"P1","P2","P3","P4"};
        String[] tags = new String[]{"T1","T2","T3","T4"};
        Tags tag = new Tags();
        tag.attachTagToProduct("T1","P1");
        tag.attachTagToProduct("T1","P4");
        tag.attachTagToProduct("T2","P1");
        tag.attachTagToProduct("T2","P2");
        tag.attachTagToProduct("T2","P4");
        tag.attachTagToProduct("T3","P2");
        tag.attachTagToProduct("T3","P4");
        tag.attachTagToProduct("T4","P3");
        tag.attachTagToProduct("T4","P4");
        System.out.println("All Products having tag T3 are ");
        tag.getProductsByTag("T3").stream().forEach(x->System.out.print(x));
        System.out.println("");
        System.out.println("All Products having tag T2 are ");
        tag.getProductsByTag("T2").stream().forEach(x->System.out.print(x));
        System.out.println("");
        System.out.println("All Tags for product P1 are  ");
        tag.getTagsAssociatedToProduct("P1").stream().forEach(x->System.out.print(x));
        System.out.println("");
        System.out.println("All Tags for product P4 are  ");
        tag.getTagsAssociatedToProduct("P4").stream().forEach(x->System.out.print(x));
        System.out.println("");



    }
}
