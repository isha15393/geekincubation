package misc;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Queue;

public class SnakeGame {

    private int width;
    private int height;
    private int[][] food;
    private int score;
    private Deque<String > body;
    private Queue<String> foodQ;

    public SnakeGame(int width, int height, int[][] food) {
        this.width = width;
        this.height = height;
        this.food = food;
        this.score = 0;
        body = new ArrayDeque();
        body.add("0,0");
        foodQ = new ArrayDeque();
        for(int i=0;i<food.length;i++) {
            String val = "";
            for (int j = 0; j < food[0].length; j++) {
                val = val + food[i][j];
            }
            String foodPoint
                    = val.charAt(0) + "," + val.charAt(1);
            foodQ.add(foodPoint);
        }

    }

    public int move(String direction){
        String curr = body.peekLast();
        int headX = Integer.parseInt(curr.split(",")[0]);
        int headY = Integer.parseInt(curr.split(",")[1]);
        switch (direction) {
            case "U" : headX--;break;
            case "L" : headY--;break;
            case "D" : headX++;break;
            case "R" : headY++;break;
            default: return -1;
        }

        if(headX<0 || headX > height-1 || headY <0 || headY>width-1) {
            return -1;
        }

        String currentStep = headX+","+headY;
        if(currentStep.equals(foodQ.peek())) {
            foodQ.poll();
            score++;
            body.addLast(currentStep);
        }else{
            body.pollFirst();
            if(body.contains(currentStep))
                return -1;
            else
                body.addLast(currentStep);
        }

        return score;
    }

    public static void main(String[] args) {
        SnakeGame snakeGame = new SnakeGame(3,2,new int[][]{{1,2},{0,1}});
        System.out.println("R-"+snakeGame.move("R"));
        System.out.println("D-"+snakeGame.move("D"));
        System.out.println("R-"+snakeGame.move("R"));
        System.out.println("U-"+snakeGame.move("U"));
        System.out.println("L-"+snakeGame.move("L"));
        System.out.println("U-"+snakeGame.move("U"));


    }
}
