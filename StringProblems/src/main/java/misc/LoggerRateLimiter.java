package misc;

import java.util.Hashtable;
import java.util.Map;

public class LoggerRateLimiter {

    static class Logger{

        Map<String,Integer> msgTimestampMap;
        int limit;

        public Logger(int limit){
            msgTimestampMap = new Hashtable<>();
            this.limit = limit;
        }

        public boolean shouldPrintMessage(int timestamp, String message) {
            if(msgTimestampMap.containsKey(message)) {
                if((timestamp-msgTimestampMap.get(message))>=limit) {
                    msgTimestampMap.put(message,timestamp);
                    return true;
                }else{
                    return false;
                }
            }else
            {
                msgTimestampMap.put(message,timestamp);
                return true;
            }
        }
    }

    public static void main(String[] args) {
        Logger logger = new Logger(10);
        System.out.println("Will the message 'foo' print "+logger.shouldPrintMessage(1,"foo"));
        System.out.println("Will the message 'bar' print "+logger.shouldPrintMessage(2,"bar"));
        System.out.println("Will the message 'foo' print "+logger.shouldPrintMessage(3,"foo"));
        System.out.println("Will the message 'bar' print "+logger.shouldPrintMessage(8,"bar"));
        System.out.println("Will the message 'foo' print "+logger.shouldPrintMessage(10,"foo"));
        System.out.println("Will the message 'foo' print "+logger.shouldPrintMessage(11,"foo"));



    }
}
