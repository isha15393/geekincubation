package misc;

import java.util.*;
import java.util.stream.Collectors;

public class voteCount {

    public static String rankTeams(String[] votes) {
        Map<Integer, Map<Character, Integer>> voteMap = new HashMap();
        int total_candidate = 3;
        for (int i = 0; i < votes.length; i++) {
            String vote = votes[i];
            char[] voteArr = vote.toCharArray();
            for (int j = 0; j < total_candidate; j++) {
                char votec = voteArr[j];
                if (voteMap.containsKey(j)) {
                    Map<Character, Integer> map = voteMap.get(j);
                    if (map.containsKey(votec)) {
                        map.put(votec, map.get(votec) + 1);
                    } else {
                        map.put(votec, 1);
                    }
                } else {
                    Map<Character, Integer> map = new HashMap();
                    map.put(votec, 1);
                    voteMap.put(j, map);
                }
            }
        }

        StringBuffer result = new StringBuffer();
        for(int k=0;k<total_candidate;k++) {
            Map<Character,Integer> map = voteMap.get(k);
            int v = map.entrySet().stream().max(Comparator.comparing(Map.Entry::getValue)).get().getValue();
            List<Character> val = map.entrySet().stream().filter(x->x.getValue().equals(v)).map(x->x.getKey()).collect(Collectors.toList());
            if(val.size() == 1) {
                result.append(val.get(0));
            }else {

            }
        }
        return result.toString();
    }

    public static String rankTeams2(String[] votes) {
        if(votes == null || votes.length == 0){
            return null;
        }

        if(votes.length == 1){
            return votes[0];
        }

        // save to map
        Map<String, VoteRecord> map = new HashMap<String, VoteRecord>();
        for(String vote : votes){
            for(int i = 0; i < vote.length(); i++){
                String team = String.valueOf(vote.charAt(i));
                VoteRecord record = map.getOrDefault(team, new VoteRecord(team, vote.length()));
                record.ranks[i]++;
                map.put(team, record);
            }
        }

        // sort in list
        List<VoteRecord> list = new ArrayList<>(map.values());
        Collections.sort(list, new Comparator<VoteRecord>() {
            @Override
            public int compare(VoteRecord o1, VoteRecord o2) {
                int idx = 0;
                while(idx < o1.ranks.length && idx < o2.ranks.length){
                    if(o1.ranks[idx] == o2.ranks[idx]){
                        idx++;
                    }else{
                        return o2.ranks[idx] - o1.ranks[idx];
                    }
                }
                //If two or more teams are still tied after considering all positions, we rank them alphabetically based on their team letter.
                return o1.team.compareTo(o2.team);
            }
        });

        //build result
        StringBuilder res = new StringBuilder();
        for(VoteRecord record : list){
            res.append(record.team);
        }

        return res.toString();
    }
    public static void main(String[] args) {
        //System.out.println("Winner rank is "+rankTeams2(new String[]{"ABC","ACB","ABC","ACB","ACB"}));
        System.out.println("Winner rank is "+rankTeams2(new String[]{"WXYZ","XYZW"}));
      //  System.out.println("Winner rank is "+rankTeams(new String[]{"ABC"}));

    }
}
