package misc;

public class VoteRecord {

        String team;
        int[] ranks;

        public VoteRecord(String team, int teamCount){
            this.team = team;
            this.ranks = new int[teamCount];
        }

}
