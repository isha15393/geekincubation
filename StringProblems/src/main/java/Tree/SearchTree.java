package Tree;
public class SearchTree {
    public static void main(String[] args) {
       Node node = new Node();
        node =  node.add(null,50);
        node = node.add(node,30);
        node = node.add(node,20);
        node = node.add(node,40);
        node = node.add(node,70);
        node = node.add(node,60);
        node = node.add(node,80);
        System.out.println("The tree [50,30,20,40,70,60,80] contains 70 - " +node.isContains(70,node));
        System.out.println("The tree [50,30,20,40,70,60,80] contains 100 - " +node.isContains(100,node));
        System.out.println("The tree [50,30,20,40,70,60,80] contains 20 - " +node.isContains(20,node));
    }
}
