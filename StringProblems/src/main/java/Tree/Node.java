package Tree;

public class Node {

        int data;
        Node left;
        Node right;

        public Node add(Node head, int data) {
            if(head == null) {
                head = new Node();
                head.data = data;
                return head;
            }

            if(head != null) {
                Node node = new Node();
                node.data = data;
                if(data < head.data) {
                    head.left = add(head.left,data);
                } else {
                    head.right = add(head.right,data);
                }
            }
            return head;
        }

        public static Node getTree() {
            Node node = new Node();
            node =  node.add(null,3);
            node = node.add(node,1);
            node = node.add(node,2);
            node = node.add(node,5);
            node = node.add(node,7);
            node = node.add(node,6);
            return node;
        }
        public boolean isContains(int data,Node head) {
            if(head == null || (head.data != data && head.left==null && head.right ==null))
                return false;
            else if(data< head.data) {
                return isContains(data,head.left);
            }else if(data > head.data) {
                return isContains(data, head.right);
            }else {
                return true;
            }
        }

        public void printInorderTraversal(Node head) {
            if(head != null) {
                printInorderTraversal(head.left);
                System.out.print(head.data + " ");
                printInorderTraversal(head.right);
            }
        }
    public void printPostorderTraversal(Node head) {
        if (head != null) {
            printPostorderTraversal(head.left);
            printPostorderTraversal(head.right);
            System.out.print(head.data + " ");
        }
    }
    public void printPreorderTraversal(Node head) {
        if (head != null) {
            System.out.print(head.data + " ");
            printPreorderTraversal(head.left);
            printPreorderTraversal(head.right);
        }
    }

    public int heightOfTree(Node head) {
            if(head == null)
                return -1;
            if(head.left == null && head.right == null)
                return 1;
            return Math.max(heightOfTree(head.left),heightOfTree(head.right)) + 1;
    }
}
