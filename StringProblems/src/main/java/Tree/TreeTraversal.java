package Tree;

public class TreeTraversal {
    public static void main(String[] args) {
        Node node = new Node();
        node =  node.add(null,3);
        node = node.add(node,1);
        node = node.add(node,2);
        node = node.add(node,5);
        System.out.println("pre order Traversal for tree[3,1,2,5] is ");
        node.printPreorderTraversal(node);
        System.out.println("In order Traversal for tree[3,1,2,5] is ");
        node.printInorderTraversal(node);
        System.out.println("post order Traversal for tree[3,1,2,5] is ");
        node.printPostorderTraversal(node);

        node = new Node();
        node =  node.add(null,50);
        node = node.add(node,30);
        node = node.add(node,20);
        node = node.add(node,40);
        node = node.add(node,70);
        node = node.add(node,60);
        node = node.add(node,80);
        System.out.println("");
        System.out.println("pre order Traversal for tree[50,30,20,40,70,60,80] is ");
        node.printPreorderTraversal(node);
        System.out.println("In order Traversal for tree[50,30,20,40,70,60,80] is ");
        node.printInorderTraversal(node);
        System.out.println("post order Traversal for tree[50,30,20,40,70,60,80] is ");
        node.printPostorderTraversal(node);
    }
}
