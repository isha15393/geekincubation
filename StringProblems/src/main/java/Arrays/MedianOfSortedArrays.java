package Arrays;

public class MedianOfSortedArrays {

    public static double findMedianOfTwoSortedArrays(int[] arr1,int arr2[]) {
        int len = arr1.length + arr2.length;
        int k = len/2;
        int i = 0;
        int p1 = 0; int p2 = 0;
        int result = -1;
        int prev = 0;
        while(i<=k && p1<arr1.length && p2<arr2.length) {
           if(arr1[p1] < arr2[p2]) {
                    prev = result;
                    result = arr1[p1];
                    i++;
                    p1++;
                } else {
                    prev = result;
                    result = arr2[p2];
                    i++;
                    p2++;
                }
            }
            if(i<=k && p1<arr1.length) {
                prev = result;
                result = arr1[p1];
                i++;
            }
            if(i<=k && p2<arr2.length) {
                prev = result;
                result = arr2[p2];
                i++;
            }
        if(len%2 != 0)
        return result;
        else
            return (prev+result)/2.0;
    }
    public static void main(String[] args) {
        System.out.println("Median of two sorted arrays is "+findMedianOfTwoSortedArrays(new int[]{1,7,8,10},new int[]{2,4,5,6}));
        System.out.println("Median of two sorted arrays is "+findMedianOfTwoSortedArrays(new int[]{1,7,8,10},new int[]{2,4,5}));
    }
}
