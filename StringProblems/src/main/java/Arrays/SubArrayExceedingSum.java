package Arrays;

public class SubArrayExceedingSum {

    public static int lengthOfSubArray(int[] arr, int k) {
        int tempSum = 0;
        int start = -1; int end = -1; int length = -1;
        int i = -1;
        while(i<arr.length){
         i = end + 1;

            start = i;
            int n = i;
            while (tempSum <= k && n<arr.length) {
                tempSum = tempSum + arr[n];
                end++;
                n++;
            }
            while (tempSum > k) {
                tempSum = tempSum - arr[start];
                start++;
            }
            int diff = end - start;
            if (length > diff) {
                length = diff;
            }
            tempSum = 0;
        }

        return length;
    }
    public static void main(String[] args) {
        System.out.println("The Length of the subArray [1,4,45,6,0,19] is "+lengthOfSubArray(new int[]{1,4,45,6,0,19},51));
    }
}
