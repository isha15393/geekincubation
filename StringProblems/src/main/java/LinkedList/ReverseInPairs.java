package LinkedList;

public class ReverseInPairs {

    static class Node{
       public int data;
       public Node next;

        public Node(){}

        static public Node addNode(Node head,int data){
            Node newnode = new Node();
            newnode.data = data;
            if(head == null) {
                return newnode;
            }
            head.next=newnode;
            return newnode;
        }

        public static Node createLinkedList(){
            Node head = new Node();
            head.data = 1;
            Node node = Node.addNode(head,2);
            node = Node.addNode(node,3);
            node = Node.addNode(node,4);
            node = Node.addNode(node,5);
            node = Node.addNode(node,6);
            node = Node.addNode(node,7);
            return head;
        }

        static public Node reverseLinkedList(Node head) {
            if(head == null || head.next == null) {
                return head;
            }else{
                Node p = null;
                Node q = head;
                Node r =null;
                while(q!=null) {
                    r=q.next;
                    q.next = p;
                    p=q;
                    q=r;
                }
                head=p;
            }
            return head;
        }

        static public Node reverseLinkedListInPairs(Node head) {
            if(head == null || head.next == null) {
                return head;
            }else {
                Node p = head;
                while( p!=null && p.next!=null) {
                    int tempData = p.next.data;
                    p.next.data = p.data;
                    p.data = tempData;
                    p= p.next.next;
                }
            }
            return head;
        }
    }

    public static void main(String args[]){
        Node node = Node.createLinkedList();
        node = Node.reverseLinkedListInPairs(node);
        while (node!=null) {
            System.out.println(node.data);
            node = node.next;
        }
    }
}
