package LinkedList;

public class ReverseLL {

    public static void main(String args[]){
        ReverseInPairs.Node head = ReverseInPairs.Node.createLinkedList();
        head = ReverseInPairs.Node.reverseLinkedList(head);
        while (head!=null) {
            System.out.println(head.data);
            head = head.next;
        }

    }
}
