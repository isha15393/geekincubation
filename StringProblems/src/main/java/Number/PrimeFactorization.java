package Number;

public class PrimeFactorization {

    public static void printPrimeFactors(int number) {
        while(number % 2 == 0) {
            System.out.println("2 ");
            number = number/2;
        }

        for(int i=3;i<Math.sqrt(Double.valueOf(number));i=i+2) {
            while (number%i == 0)
            {
                System.out.print(i + " ");
                number /= i;
            }
        }

        if (number > 2)
            System.out.print(number);
    }

    public static void main(String[] args) {
        System.out.println("The Prime Factors of number 315 - ");
        printPrimeFactors(315);
        System.out.println("The Prime Factors of number 27 - ");
        printPrimeFactors(27);
    }
}
