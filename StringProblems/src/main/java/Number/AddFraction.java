package Number;

public class AddFraction {

    private static int calcaulateGcd(int a, int b) {
        if(a == 0)
            return b;
        return calcaulateGcd(b%a,a);
    }
    public static String addFractions(int num1, int den1, int num2, int den2) {
        int gcd = calcaulateGcd(den1,den2);
        int lcm = (den1 * den2)/gcd;
        int num = num1 *(lcm/den1) + num2 *(lcm/den2);
        int gcd2 = calcaulateGcd(num,lcm);
        num= num/gcd2;
        lcm = lcm/gcd2;
        return num+"/"+lcm;
    }

    public static void main(String[] args) {
        System.out.println("The Sum of fractions [5,4] and [3,2] is "+addFractions(5,4,3,2));
        System.out.println("The Sum of fractions [1,4] and [7,4] is "+addFractions(1,4,7,4));
        System.out.println("The Sum of fractions [2,3] and [1,2] is "+addFractions(2,3,1,2));
    }
}
