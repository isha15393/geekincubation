package Number;

public class DotProduct {

    public static int calculateDotProduct(int[] arr1, int[] arr2) {
        int result = 0;

        for(int i=0;i<arr1.length;i++) {
            result = result + (arr1[i] * arr2[i]);
        }
        return result;
    }
    public static void main(String[] args) {
        System.out.println("Dot Product of arr[2,3] and [1,4] is "+calculateDotProduct(new int []{2,3},new int[]{1,4}));
        System.out.println("Dot Product of arr[1,2] and [2,3] is "+calculateDotProduct(new int []{1,2},new int[]{2,3}));
    }
}
