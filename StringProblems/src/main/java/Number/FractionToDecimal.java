package Number;

/*
* Given two integers representing the Numerator and Denominator of a fraction,
* return the fraction in string format. If the fractional part is repeating,
* enclose the repeating part in parentheses.
* */

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class FractionToDecimal {

    public static String getDecimalConversion(long numer, long denom) {
        long num = Math.abs(numer);
        long den = Math.abs(denom);
        StringBuffer result = new StringBuffer();
        long quotient = num/den;
        result.append(quotient);
        long remainder = num % den * 10;
        Map<Long,Integer> remMap = new HashMap<>();
        if(remainder == 0) {
            return result.toString();
        }
        result.append(".");
        while(remainder!=0) {

            if(remMap.containsKey(remainder)) {
                    String part1 = result.substring(0,remMap.get(remainder));
                    String part2 = result.substring(remMap.get(remainder),result.length());
                    String part3 = "("+part2+")";
                    return  part1 + part3;
            }
            remMap.put(remainder,result.length());
            quotient = remainder/den;
            result.append(quotient);
            remainder = remainder % den * 10;
        }
        return result.toString();
    }
    public static void main(String[] args){
        System.out.println("Decimal conversion of fraction 8/3 is "+getDecimalConversion(8L,3L));
        System.out.println("Decimal conversion of fraction 50/22 is "+getDecimalConversion(50L,22L));

    }
}
