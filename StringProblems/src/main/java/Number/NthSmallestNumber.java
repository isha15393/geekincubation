package Number;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class NthSmallestNumber {

    public static int findMinimumElement(int pos, int[] arr) {
        Stack<Integer> stack = new Stack<>();

        for(int j=0;j<pos;j++){
            if(stack.isEmpty() || stack.peek()<arr[j])
            stack.push(arr[j]);
            else {
                List<Integer> num = new ArrayList<>();
                while(stack.peek()>arr[j]) {
                    num.add(stack.pop());
                }
                stack.push(arr[j]);
                num.forEach(x->stack.push(x));
            }
        }

        for(int i=pos;i<arr.length;i++) {
            if(arr[i] > stack.peek()){
                continue;
            } else if(arr[i] < stack.peek()) {
                List<Integer> tempList = new ArrayList<>();
                while(!stack.isEmpty() && arr[i] < stack.peek()) {
                    tempList.add(stack.pop());
                }
                int k = tempList.size()-1;
                stack.push(arr[i]);
                while(stack.size()<pos) {
                    stack.push(tempList.get(k));
                    k--;
                }
            }
        }
        return stack.pop();
    }
    public static void main(String[] args) {
        System.out.println("The Second smallest element in [-1,2,0,1,3] is "+findMinimumElement(2,new int[]{-1,2,0,1,3}));
        System.out.println("The Third smallest element in [-1,2,0,1,3] is "+findMinimumElement(3,new int[]{-1,2,0,1,3}));
        System.out.println("The Second smallest element in [-2,2,-1,1,0,3,-3] is "+findMinimumElement(2,new int[]{-2,2,-1,1,0,3,-3}));
        System.out.println("The fourth smallest element in [-2,2,-1,1,0,3,-3] is "+findMinimumElement(4,new int[]{-2,2,-1,1,0,3,-3}));

    }

}
