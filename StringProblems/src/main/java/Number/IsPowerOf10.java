package Number;

public class IsPowerOf10 {

    public static boolean isPowerOf10(int x) {
        if(x<10) return false;
        if(x%10 == 0) return true;
        return false;
    }
    public static void main(String[] args) {
        System.out.println("Is 10 power of 10 - "+isPowerOf10(10));
        System.out.println("Is 2 power of 10 - "+isPowerOf10(2));
        System.out.println("Is 1000 power of 10 - "+isPowerOf10(1000));
    }
}
