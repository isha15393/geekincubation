package Number;

public class PowerOfExponents {

    public static double power(double base, int exp) {
        double output = base;
        exp--;
        while(exp>0) {
            output = output * base;
            exp --;
        }
        return output;
    }

    public static void main(String[] args) {
        System.out.println("Exponent 2 raised to power 4 is "+power(2,4));
        System.out.println("Exponent 3 raised to power 3 is "+power(3,3));

    }
}
