package Number;

public class SmallestNumberInRotatedArray {

    //without duplicates
    public static int  findMin(int[] arr,int i,int j) {
        int mid = (i + j) / 2;
        if(i==j)
            return arr[i];
        if (i < j) {
            if (arr[mid] < arr[mid + 1]) {
                if(mid == 0 || !(arr[mid] > arr[mid-1]))
                return arr[mid];
            }

        }
        return Math.min(findMin(arr,i,mid),findMin(arr,mid+1,j));
    }

    public static void main(String[] args) {
        System.out.println("The minimum number in rotated arr is "+findMin(new int[]{3,4,5,6,1,2},0,5));
        System.out.println("The minimum number in rotated arr is "+findMin(new int[]{3,4,5,6,7,2},0,5));
        System.out.println("The minimum number in rotated arr is "+findMin(new int[]{3,4,5,6,7,8},0,5));

    }

}
